# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import copy
import restkit
import time
import datetime
from urlparse import urljoin
from trytond.model import ModelView, ModelSQL, fields
from trytond.modules.ticket_tracker import TrackerHandler
from trytond.pool import Pool


_RT_STATUS_MAP = {
    #rt-status: tryton-states
    'open': 'open',
    'new': 'open',
    'stalled': 'open',
    'resolved': 'closed',
    'rejected': 'closed',
    'deleted': 'closed',
}

class Tracker(ModelSQL, ModelView):
    _name = 'ticket.tracker'

    rest_path = fields.Char('REST Path', help='The relative path to the '
        'REST interface of the Tracker connection.')
    query = fields.Char('Query', help='The query to get the ticket data.')

    def __init__(self):
        super(Tracker, self).__init__()
        self.type = copy.copy(self.type)
        if ('rt', 'Request Tracker (RT)') not in self.type.selection:
            self.type.selection += [('rt', 'Request Tracker (RT)')]
        self._reset_columns()

    def _check_update(self, tracker_ticket, type):
        res = super(Tracker, self)._check_update(tracker_ticket, type)
        if type == 'rt':
            res = self._check_update_rt(tracker_ticket)
        return res

    def _check_update_rt(self, rt_ticket):
        ticket_obj = Pool().get('ticket.work')

        res = True
        # Match rt ticket and tryton ticket, if exists.
        tryton_ticket = ticket_obj.search_read([
            ('tracker_code', '=', rt_ticket['id']),
        ], limit=1, fields_names=['tracker_last_change', 'last_sync_state'])
        if (tryton_ticket and tryton_ticket['tracker_last_change'] and
                tryton_ticket['tracker_last_change'] >=
                datetime.datetime(
                    *time.strptime(rt_ticket['LastUpdated'])[0:5]) and
                tryton_ticket['last_sync_state'] == 'ok'):
            res = False
        return res

    def map_tryton_ticket(self, data, type):
        res = super(Tracker, self).map_tryton_ticket(data, type)
        if type == 'rt':
            tryton_ticket_id, tryton_ticket_data, message, sync_state, \
                tryton_ticket_state = self._map_tryton_ticket_rt(data)
            tryton_ticket_data, message, tryton_ticket_state, sync_state = \
                self._check_state_rt(tryton_ticket_data, message,
                    tryton_ticket_state, sync_state)
        return (tryton_ticket_id, tryton_ticket_data, message.strip(),
            sync_state)

    def _map_tryton_ticket_rt(self, rt_ticket):
        employee_obj = Pool().get('company.employee')
        ticket_obj = Pool().get('ticket.work')

        tryton_ticket_data = {}
        message = ''
        sync_state = 'ok'
        # General map rt to tryton
        for _map in self.ticket2tryton():
            tryton_ticket_data[_map[1]] = rt_ticket.get(_map[0]) or _map[2]

        # Map rt status to tryton status
        if rt_ticket.get('Status'):
            tryton_ticket_state = _RT_STATUS_MAP.get(
                rt_ticket['Status'], 'open')
            tryton_ticket_data['state'] = tryton_ticket_state

        if rt_ticket.get('LastUpdated'):
            tryton_ticket_data['tracker_last_change'] = datetime.datetime(
                *time.strptime(rt_ticket['LastUpdated'])[0:5])

        # Map RT Owner to tryton assigned_to
        assigned_to = None
        if rt_ticket.get('Owner'):
            employee = employee_obj.search([
                ('rt_user', '=', rt_ticket['Owner']),
                ])
            if employee:
                if isinstance(employee, dict):
                    employee = [employee]
                if len(employee) == 1:
                    assigned_to = employee[0]
        if not assigned_to:
            # Can not close the ticket, when there is no owner
            message += ('MATCH ERROR: Can not find a Tryton employee '
                'for RT Owner "%s"\n' % (rt_ticket['Owner'],))
            sync_state = 'not_ok'
            tryton_ticket_state = 'open'
        tryton_ticket_data['assigned_to'] = assigned_to

        requestor_id, message = self._map_requestor_rt(rt_ticket, message)
        if not requestor_id:
            sync_state = 'not_ok'
            tryton_ticket_state = 'open'
        tryton_ticket_data['requestor'] = requestor_id

        # Match rt ticket and tryton ticket, if exists.
        tryton_ticket_id = ticket_obj.search([
            ('tracker_code', '=', rt_ticket['id']),
            ], limit=1)
        if tryton_ticket_id and isinstance(tryton_ticket_id, list):
            tryton_ticket_id = tryton_ticket_id[0]

        return (tryton_ticket_id or None, tryton_ticket_data, message,
                sync_state, tryton_ticket_state)

    def _map_requestor_rt(rt_ticket, message):
        contact_mechanism_obj = Pool().get('party.contact_mechanism')
        party_obj = Pool().get('party.party')

        requestor_id = None
        rt_requestor = rt_ticket.get('Requestors')
        if not rt_requestor:
            message += ('MATCH ERROR: No RT requestor set on this ticket!\n')
        while rt_requestor:
            rt_requestor = rt_requestor.strip(',')
            if ',' in rt_requestor:
                message += ('MATCH ERROR: Multiple RT requestors! Can not '
                    'match many requestors to a single party: %s\n' % (
                        rt_ticket['Requestors'],))
                break
            # Step1: Check for a party with the email address:
            contact_values = contact_mechanism_obj.search_read([
                ('value', 'ilike', rt_requestor),
                ('type', '=', 'email'),
                ], fields_names=['party'])
            if contact_values:
                if len(contact_values) == 1:
                    requestor_id = contact_values[0].get('party')
                else:
                    message += ('MATCH ERROR: Multiple parties match RT '
                        'requestor! Parties need to have unique email '
                        'addresses for automated assignment.\n')
                break
            # Step2: Check for an organization with the email domain
            _, requestor_email_domain = rt_requestor.split('@')
            organization_ids = party_obj.search([
                ('party_type', '=', 'organization')])
            contact_values = contact_mechanism_obj.search_read([
                ('value', 'ilike', '%'+requestor_email_domain),
                ('type', '=', 'email'),
                ('party', 'in', organization_ids)
                ], fields_names=['party'])
            if contact_values:
                if len(contact_values) == 1:
                    requestor_id = contact_values[0].get('party')
                    requestor = party_obj.browse(requestor_id)
                    message += ('MATCH INFO: Fuzzy match RT requestor %s '
                        'to organization "%s" with code "%s"\n' % (
                            rt_requestor, requestor.full_name, requestor.code))
                else:
                    message += ('MATCH ERROR: Multiple organizations match '
                        'RT requestor email domain "%s"! '
                        'Organizations need to have unique email '
                        'domains for automated assignment.\n' %
                        (requestor_email_domain,))
            else:
                message += ('MATCH ERROR: Can not match RT requestor %s\n' % (
                    rt_requestor,))
            break
        return requestor_id, message

    def ticket2tryton(self):
        return [
            # rt-variable, tryton-variable, default value
            ('id', 'tracker_code', None),
            ('Subject', 'name', '--'),
            ]

    def _check_state_rt(self, tryton_ticket_data, message, tryton_ticket_state,
            sync_state):
        if tryton_ticket_data['state'] != tryton_ticket_state:
            message += ('Can not set Tryton ticket state to '
                '"%s" because of previous problems.\n' % (
                    tryton_ticket_data['state'],))
            sync_state = 'not_ok'
            tryton_ticket_data['state'] = tryton_ticket_state
        return (tryton_ticket_data, message, tryton_ticket_state, sync_state)

Tracker()


class RtHandler(TrackerHandler):
    'RT Request Tracker Handler'
    _name = 'rt'

    def __init__(self, tracker):
        super(RtHandler, self).__init__()
        self.config = tracker
        self.login = tracker.login
        self.password = tracker.password
        self.rest_path = tracker.rest_path
        self.query = tracker.query
        self.url = tracker.url

    def check_connection(self):
        if self.login:
            path = '/user/' + self.login
            check = self._get(path)
            if check:
                return True
        return False

    def _get(self, path):
        res = self._connect()
        if res:
            path = '/' + self.rest_path + path
            res = res.get(path=path)
            status = res.status_int
            if status == 200:
                return res.body_string()
        return ''

    def _connect(self):
        filters = []
        res = None
        if self.login and self.password:
            auth = restkit.BasicAuth(self.login, self.password)
            filters.append(auth)
        if self.url and self.rest_path:
            #resp = restkit.request(conf.rt_url, )
            r = restkit.Resource(self.url+'/', filters=filters)
            if r.clone().get().status_int == 200:
                res = r
        return res

    def get_tickets(self):
        path = urljoin(self.rest_path, 'search/ticket')
        query = self.query or ''
        format = 'l'
        # Map Url to rest Api
        conn = self._connect()
        res = conn.get(path=path, query=query, format=format).body_string()
        # Filter res for tickets
        raw_tickets = [t for t in res.split('--\n')]
        # Map tickets to a list of dictionaries:
        tickets = []
        for idx, raw_ticket in enumerate(raw_tickets):
            if raw_ticket == '\n' or ':' not in raw_ticket:
                continue
            ticket = raw_ticket.split('\n')
            tickets.append({})
            # Map variable: value to dictionary:
            for line in ticket:
                if ':' not in line:
                    continue
                key, val = line.split(':', 1)
                val = val.strip()
                if key == 'id':
                    val = val.lstrip('ticket/')
                tickets[idx][key] = val
        return tickets or []

    def get_backlink(self, code):
        callback_url = ''
        if code:
            callback_url = "%s/Ticket/Display.html?id=%s" % (
                self.url.rstrip('/'), code)
        return callback_url
