# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields


class Employee(ModelSQL, ModelView):
    _name = 'company.employee'

    rt_user = fields.Char('RT User', help='The request tracker username '
        'of this employee.')

Employee()

