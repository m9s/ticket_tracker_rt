# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Ticket Tracker RequestTracker',
    'name_de_DE': 'Ticket Ticketsystem-Anbindung RequestTracker',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Extends the ticket_tracker module to synchronize with
      the RequestTracker[1] application

    [1] http://www.bestpractical.com/rt/
    ''',
    'description_de_DE': '''
    - Erweitert die Funktion des ticket_tracker Moduls um die
      Synchronisation mit der RequestTracker[1] Anwendung.

    [1] http://www.bestpractical.com/rt/
    ''',
    'depends': [
        'ticket_tracker',
    ],
    'xml': [
        'company.xml',
        'tracker.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
